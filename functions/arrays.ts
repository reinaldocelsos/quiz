export default function embaralhar(elementos: any[]) : any[]{
    return elementos.map(valor => ({valor, elemento: Math.random()}))
    .sort((obj1, obj2)=> obj1.elemento - obj2.elemento)
    .map(el=> el.valor)
}
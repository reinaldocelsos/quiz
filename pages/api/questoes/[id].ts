import questoes from "../BancoDeDadosQuestoes";

export default function handler(req, res) {
    const idSelecionado = +req.query.id
    const questao = questoes.filter(q => q.id === idSelecionado)   
     if (questao.length > 0){
         const resposta = questao[0].embaralharRespostas()
         res.status(200).json(resposta.paraObjeto())
     }else{
         res.status(204).send()
     }
  }
  
import QuestaoModelo from "../../modelo/questao";
import RespostaModelo from "../../modelo/resposta";

const questoes : QuestaoModelo[] = [
    new QuestaoModelo(201, "Inseto que transmite chagas? ", [
        RespostaModelo.errada("Abelha"),
        RespostaModelo.errada("Aranha"),
        RespostaModelo.errada("Borboleta"),
        RespostaModelo.certa("Barbeiro")
    ]),
    new QuestaoModelo(202, "Time da serie B? ", [
        RespostaModelo.errada("São Paulo"),
        RespostaModelo.errada("Bragantino"),
        RespostaModelo.errada("Bahia"),
        RespostaModelo.certa("Cruzeiro")
    ]),
    new QuestaoModelo(203, "País da zona do euro? ", [
        RespostaModelo.errada("Afeganistão"),
        RespostaModelo.errada("Brasil"),
        RespostaModelo.errada("Japão"),
        RespostaModelo.certa("Turquia")
    ]),
    new QuestaoModelo(204, "Planta do cerrado? ", [
        RespostaModelo.errada("Orquidia"),
        RespostaModelo.errada("Sarça"),
        RespostaModelo.errada("Flamboyant"),
        RespostaModelo.certa("Ipê")
    ]),
]
export default questoes;
import embaralhar from "../../../functions/arrays";
import bancoDeDados from "../BancoDeDadosQuestoes"
export default (req,res)=>{
    const questao = bancoDeDados;
    const ids = questao.map(q=>q.id)
    return res.status(200).json(embaralhar(ids));
}
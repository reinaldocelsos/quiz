import { useState } from 'react'
import Questao from '../components/Questao'
import QuestaoModelo from '../modelo/questao'
import RespostaModelo from '../modelo/resposta'

const questoesMok = 
  new QuestaoModelo(201, "Inseto que transmite chagas? ", [
      RespostaModelo.errada("Abelha"),
      RespostaModelo.errada("Aranha"),
      RespostaModelo.errada("Borboleta"),
      RespostaModelo.certa("Barbeiro")])
export default function Home() {
  const [questao, setquestao]= useState(questoesMok)
  function respostaRespondida(indice : number) {
    setquestao (questao.responderCom(indice))
  }
  function tempoEsgotado() {
    if (questao.naoRespondida)
      setquestao (questao.responderCom(-1))
  }
    
  return (
    <div style={{display:"flex", height:"100vh", alignItems:"center", justifyContent:"center"}}>
      <Questao valor = {questao} respostaRespondida={respostaRespondida} TempoEsgotado={tempoEsgotado} tempoResposta = {5}></Questao>

    </div>
  )
}

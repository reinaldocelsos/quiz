import embaralhar from "../functions/arrays"
import RespostaModelo from "./resposta"

export default class QuestaoModelo{
    #id : number
    #enunciado : string
    #resposta : RespostaModelo[]
    #acertou : boolean
    
    constructor (id:number, enunciado : string, resposta : RespostaModelo[], acertou = false){
        this.#id =id
        this.#enunciado =enunciado
        this.#resposta =resposta
        this.#acertou =acertou
    }
   

    get id(){
        return this.#id
    }
    get enunciado(){
        return this.#enunciado
    }
    get resposta(){
        return this.#resposta
    }
    get acertou(){
        return this.#acertou
    }
    get naoRespondida(){
        return ! this.resposta
    }
    get respondida(){
        for (let resp of this.#resposta){
            if (resp.revelada) return true
        }
        return false
    }
    responderCom(indice : number): QuestaoModelo{
        const acertou = indice > 0 ? this.#resposta[indice].certa : false;
        const respostas = this.#resposta.map((resposta, i)=>{
            const respostaSelecionada = indice === i
            const deveRevelar = respostaSelecionada || resposta.certa
            return deveRevelar ? resposta.revelar() : resposta
        })
        return new QuestaoModelo(this.id, this.#enunciado, respostas, acertou)
    }
    embaralharRespostas() : QuestaoModelo{
        const respostaEmbaralhadas = embaralhar(this.#resposta)
        return new QuestaoModelo(this.#id, this.#enunciado, respostaEmbaralhadas, this.#acertou)
    }
    paraObjeto(){
        return{
            id: this.#id,
            enunciado: this.#enunciado,
            resposta: this.#resposta.map(resp=>resp.paraOjeto()),
            respondida : this.respondida,
            acertou: this.#acertou           
        }    
    }
}
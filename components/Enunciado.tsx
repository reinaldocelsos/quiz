import style from "../styles/Enunciado.module.css"

interface EnunciadoProps{
    texto : string
}
export default function Enunciado(props : EnunciadoProps)   {
    
    return(
        <div className={style.texto} >
            <h1>{props.texto}</h1>
        </div>
    )
}
import QuestaoModelo from "../modelo/questao";
import style from '../styles/questao.module.css'
import Enunciado from "./Enunciado";
import Resposta from "./resposta";
import Temporizador from "./Temporizador";
interface QuestaoProps{
    valor : QuestaoModelo,
    respostaRespondida: (indice : number) => void
    TempoEsgotado: () => void
    tempoResposta?: number
    
}

export default function Questao(props : QuestaoProps){
    const questao = props.valor
    const letras =[
        {letra:"A", cor:"#F2C866"},
        {letra:"B", cor:"#F266BA"},
        {letra:"C", cor:"#85D4F2"},
        {letra:"D", cor:"#BCE596"},
    ]
    function redenrizarResposta(){
        return questao.resposta.map((resposta, i)=>{
            return <Resposta
                key={i}
                valor={resposta}
                indice={i}
                letra={letras[i].letra}
                corLetra={letras[i].cor}    
                respostaRespondida = {props.respostaRespondida}
            />
        })
    }
    return(
        <div className={style.questao}>
           <Enunciado texto ={questao.enunciado}></Enunciado>
           <Temporizador
           tempo={props.tempoResposta?? 10}
           tempoEsgotado={props.TempoEsgotado}
           />
           {redenrizarResposta()}
        </div>
    )
}
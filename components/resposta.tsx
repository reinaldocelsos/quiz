import RespostaModelo from "../modelo/resposta";
import style from '../styles/Resposta.module.css'

interface RespostaProps {
    valor: RespostaModelo,
    indice: number,
    letra: string
    corLetra: string,
    respostaRespondida: (indice: number) => void
}
export default function Resposta(props: RespostaProps) {
    const resposta = props.valor
    return (
        <div className={style.resposta} onClick={(indice) => props.respostaRespondida(props.indice)}>
            <div className={style.conteudoResposta}>
                {! resposta.revelada ?
                    (
                        <div className={style.frente}>
                            <div className={style.letra} style={{ background: props.corLetra }} >
                                {props.letra}
                            </div>
                            <div className={style.valor}>
                                {resposta.valor}
                            </div>
                        </div>

                    ) :
                    (
                        <div className={style.verso}>
                            {resposta.certa ? (
                                <div className={style.certa}>
                                    <div>A resposta certa é </div>
                                    <div className={style.valor}>{resposta.valor}</div>
                                </div>
                            ) : (
                                <div className={style.errada}>
                                    <div>A resposta está errada </div>
                                    <div className={style.valor}>{resposta.valor}</div>
                                </div>

                            )}
                        </div>
                    )}
            </div>
        </div>
    )
}